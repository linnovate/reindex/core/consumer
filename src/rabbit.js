const amqp = require("amqplib/callback_api");
const config = require("./config");

function urlBuilder() {
  return `${config.rabbitUri}`;
}

module.exports.RabbitConnect = function() {
  return new Promise((resolve, reject) => {
    amqp.connect(urlBuilder(), (error, connection) => {
      if (error) {
        console.error(`rabbitmq faced an error while trying to connect`);

        return setTimeout(() => {
          this.RabbitConnect();
        }, 5000);
      } else {
        console.log(`rabbitmq connected successfully to rabbitMQ host`);
        connection.on("close", () => {
          console.log("amqp disconnected reconnecting....");
          return setTimeout(() => this.RabbitConnect(), 1000);
        });
      }
      return resolve(connection);
    });
  }).catch(reason => {
    return setTimeout(() => this.RabbitConnect(), 5000);
  });
};
