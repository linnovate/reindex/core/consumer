const express = require("express");
const bodyParser = require("body-parser");
const errors = require("restify-errors");
const { consumer } = require("@reindex/plugins");
const { routes } = consumer;
const config = require("./config");
const rabbit = require("./rabbit");
require("./worker");
const options = {
  port: config.port
};

global.connection = null;

const server = express();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

routes.forEach(route => {
  if (route.serviceName == process.env.SERVICE_NAME) {
    server.post(route.name, async (req, res, next) => {
      const QUEUE = route.queue;
      //create rabbit connection
      const connection = await rabbit.RabbitConnect();

      // use the connection to create channel
      await connection.createChannel((err, ch) => {
        ch.assertQueue(QUEUE);
        ch.sendToQueue(QUEUE, Buffer.from(JSON.stringify(req.body)));
      });
      return res.status(200).json({ message: "ok" });
    });
  }
});

function errorHandler(err, req, res, next) {
  // eslint-disable-line no-unused-vars

  if (err instanceof errors.HttpError) {
    res.status(err.statusCode).json(err.body);
  }

  res
    .status(500)
    .json({ code: "InternalError", message: err.message || "Fatal error" });
  return;
}

server.use(errorHandler);

server.listen(options.port, function() {
  console.log("server is listening on port", options.port);
});
